# react-workshop

## 👏 Thank you 🙌

- https://github.com/mzabriskie/react-workshop
- https://github.com/kentcdodds/react-workshop
- https://github.com/ReactTraining/react-workshop

## ✨ Install

```shell
npm install
```

**Note**: Requires [Node.js](https://nodejs.org/en/) >= 8.15. If your current Node.js version doesn't meet the requirements I recommend using [Node Version Manager](https://github.com/creationix/nvm).

## 🚀 Getting started

To start the exercises run `npm start`. Next, open
your browser and navigate to http://localhost:1234/index.html

## ✍️ Outline

1. javascript
2. imperative-vs-declarative
3. virtual-dom
4. hello-world
5. jsx
6. components
7. props
8. default-props
9. prop-types
10. styling
11. children
12. lists
13. fragments
14. conditional-rendering
15. lifecycle-methods
16. event-handlers
17. state
18. data-fetching
19. forms
20. refs
21. hoc
22. render-prop
23. hooks
24. context
25. ssr
