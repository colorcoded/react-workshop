import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 10 - Styling
 *
 * @example
 * function Example() {
 *   return (
 *     <div
 *       style={{
 *         fontFamily: 'sans-serif',
 *         fontWeight: 700,
 *       }}
 *     >
 *       Example
 *     </div>
 *   );
 * }
 *
 * @example
 * function Example() {
 *   return <div className="example">Example</div>;
 * }
 *
 * @todo
 * 👉Create a component that renders a `div` element with the text "Box" and is
 * styled using the provided `box` CSS class.
 * 👉Add a `size` property and corresponding propTypes to the component.
 * The property `size` can be "small", "medium" or "large". If the `size` is
 * "small" the component should add the `box--small` CSS class. For "medium" use
 * `box--medium` and for "large" use `box--large`. TIP: use `Template Literals`.
 * 👉Update the component and make the text "Box" configurable.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎CSS-in-JS
 * {@link https://www.styled-components.com/}
 * {@link https://emotion.sh/}
 */
