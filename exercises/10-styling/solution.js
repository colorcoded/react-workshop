import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/**
 * ✨Exercise 10 -Styling
 */

function Box({ children, size, ...props }) {
  return (
    <div {...props} className={`box box--${size}`}>
      {children}
    </div>
  );
}

Box.defaultProps = {
  size: 'medium',
};

Box.propTypes = {
  size: PropTypes.oneOf(['small', 'medium', 'large']),
};

ReactDOM.render(
  <Box size="large">Large Box</Box>,
  document.querySelector('#root')
);
