import React from './vdom/create-element';
import ReactDOM from './vdom/render';

/**
 * ✨Exercise 03 - Virtual DOM
 */

const vTree = React.createElement('ul', null, [
  React.createElement('li', null, 'Item 1'),
  React.createElement('li', null, 'Item 2'),
  React.createElement('li', null, 'Item 3'),
  React.createElement('li', null, 'Item 4'),
]);

console.log(vTree);

ReactDOM.render(vTree, document.querySelector('#root'));
