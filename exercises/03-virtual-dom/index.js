import { createElement } from './vdom/create-element';
import { render } from './vdom/render';

/**
 * ✨Exercise 03 - Virtual DOM
 * {@link https://bitsofco.de/understanding-the-virtual-dom/}
 * {@link https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05}
 *
 * @description
 * A plain object representation of the actual DOM
 *
 * @example Virtual Host Tree
 * {
 *   tagName: 'ul',
 *   children: [
 *     {
 *       tagName: 'li',
 *       children: 'Item 1'
 *     },
 *     {
 *       tagName: 'li',
 *       children: 'Item 2'
 *     },
 *     {
 *       tagName: 'li',
 *       children: 'Item 3'
 *     },
 *     {
 *       tagName: 'li',
 *       children: 'Item 4'
 *     },
 *   ]
 * }
 *
 * @todo
 * 👉Use `createElement` to create a virtual host tree (see @example) and
 * log it to your browser's console.
 * 👉Replace `import { createElement }` with `import React` and update your code
 * to reflect this change.
 * 👉Use `render` to render the virtual host tree to the DOM.
 * 👉Replace `import { render }` with `import ReactDOM` and update your code
 * to reflect this change.
 */
