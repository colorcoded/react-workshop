/**
 * {@link https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05}
 */

export function mount(node, target) {
  if (!target.childNodes.length) {
    target.appendChild(node);
  }
}
