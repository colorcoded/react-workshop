/**
 * {@link https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05}
 */

/**
 * createElement
 * @param {string} tagName HTML tag name (e.g., 'div', 'span', ...)
 * @param {object} [attrs] object defining the attributes for the element
 * @param {string|object[]} [children] the inner content of the element
 */
export function createElement(tagName, attrs, children) {
  return { tagName, attrs, children };
}

export default { createElement };
