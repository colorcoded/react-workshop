import { diff } from './diff';
import { mount } from './mount';

/**
 * {@link https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05}
 */

let vRoot;
let root;

/**
 * render
 * @param {object} vNode the virtual DOM to render
 * @param {*} target the DOM element to render to
 */
export function render(vNode, target) {
  const patch = diff(vRoot, vNode);
  vRoot = vNode;
  root = patch(root);
  mount(root, target);
}

export default { render };
