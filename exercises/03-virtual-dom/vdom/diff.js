/**
 * {@link https://dev.to/ycmjason/building-a-simple-virtual-dom-from-scratch-3d05}
 */

export function diff(oldVTree, newVTree) {
  /**
   * Create a new node if there is no current virtual tree
   */
  if (oldVTree == undefined) {
    return function() {
      const newNode = createNode(newVTree);
      return newNode;
    };
  }

  /**
   * Remove the node from the DOM if there is no new virtual tree
   */
  if (newVTree == undefined) {
    return function(node) {
      return node.remove();
    };
  }

  if (typeof oldVTree === 'string' || typeof newVTree === 'string') {
    /**
     * If both vTree's are the same string, do nothing
     */
    if (oldVTree === newVTree) {
      return function(node) {
        return node;
      };
    }

    /**
     * - If both vTree's are a string but the value is different
     * - If one of the vTree's is a string and the other an object
     */
    return function(node) {
      const newNode = createNode(newVTree);
      node.replaceWith(newNode);
      return newNode;
    };
  }

  /**
   * Replace the old node with the new node if the tagName is different.
   */
  if (oldVTree.tagName !== newVTree.tagName) {
    return function(node) {
      const newNode = createNode(newVTree);
      node.replaceWith(newNode);
      return newNode;
    };
  }

  /**
   * Patch the DOM if vTree's describe the same node
   */
  const patchAttrs = diffAttrs(oldVTree.attrs, newVTree.attrs);
  const patchChildren = diffChildren(oldVTree.children, newVTree.children);

  return function(node) {
    patchAttrs(node);
    patchChildren(node);
    return node;
  };
}

function diffAttrs(oldVAttrs, newVAttrs) {
  return function(node) {
    /**
     * Set new attributes
     */
    if (newVAttrs) {
      for (const [key, value] of Object.entries(newVAttrs)) {
        node.setAttribute(key, value);
      }
    }

    /**
     * Remove old attributes
     */
    for (const key in oldVAttrs) {
      if (!key in newVAttrs) {
        node.removeAttribute(key);
      }
    }

    return node;
  };
}

function diffChildren(oldVChildren, newVChildren) {
  return function(node) {
    /**
     * Remove unused children
     */

    /**
     * Update current children
     */
    if (Array.isArray(oldVChildren)) {
      oldVChildren.forEach(function(oldVChild, index) {
        diff(oldVChild, newVChildren[index])(node.childNodes[index]);
      });
    }

    /**
     * Append new children
     */
    if (Array.isArray(oldVChildren)) {
      for (const vChild of newVChildren.slice(oldVChildren.length)) {
        node.appendChild(createNode(vChild));
      }
    }

    return node;
  };
}

function createNode(vNode) {
  /**
   * Create TextNode if vNode is a string
   */
  if (typeof vNode === 'string') {
    return document.createTextNode(vNode);
  }

  /**
   * Create ElementNode if vNode is an object
   */
  const node = document.createElement(vNode.tagName);

  if (vNode.attrs) {
    for (const [key, value] of Object.entries(vNode.attrs)) {
      node.setAttribute(key, value);
    }
  }

  if (vNode.children) {
    for (const child of vNode.children) {
      node.appendChild(createNode(child));
    }
  }

  return node;
}
