import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 21 - HoC
 */

class FetchRandomCat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      error: null,
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({ data: null, error: null, loading: true });

    fetch('https://aws.random.cat/meow')
      .then(response => response.json())
      .then(data => this.setState({ data, error: null, loading: false }))
      .catch(error => this.setState({ data: null, error, loading: false }));
  }

  render() {
    return this.props.children({
      ...this.state,
      ...this.props,
    });
  }
}

function RandomCat() {
  return (
    <FetchRandomCat>
      {({ data, error, loading }) => {
        if (loading) {
          return <Loading />;
        }

        if (error) {
          return <Error />;
        }

        if (data) {
          return <Image src={data.file} />;
        }

        return null;
      }}
    </FetchRandomCat>
  );
}

function Loading() {
  return <p>Loading ...</p>;
}

function Error() {
  return <p>Error</p>;
}

function Image(props) {
  return <img {...props} />;
}

ReactDOM.render(<RandomCat />, document.querySelector('#root'));
