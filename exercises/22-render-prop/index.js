import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 22 - Render Prop
 * {@link https://reactjs.org/docs/render-props.html}
 * {@link https://github.com/jaredpalmer/awesome-react-render-props}
 *
 * @description
 * A declarative way to abstract state that fits with React's composition model.
 *
 * @note
 * A.k.a. function as a child.
 *
 * @example
 * class FetchNumberFacts extends React.Component {
 *   constructor(props) {
 *     super(props);
 *     this.state = {
 *       data: null,
 *       error: null,
 *       loading: false,
 *     };
 *   }
 *
 *   componentDidMount() {
 *     this.setState({ data: null, error: null, loading: true });
 *
 *     fetch('http://numbersapi.com/42')
 *       .then(response => response.text())
 *       .then(data => this.setState({ data, error: null, loading: false }))
 *       .catch(error => this.setState({ data: null, error, loading: false }));
 *   }
 *
 *   render() {
 *     return this.props.children({
 *       ...this.state,
 *       ...this.props,
 *     });
 *   }
 * }
 *
 * function NumberFact() {
 *   return (
 *     <FetchNumberFacts>
 *       {({ data, error, loading }) => {
 *         if (loading) {
 *           return <p>Loading ...</p>;
 *         }
 *
 *         if (error) {
 *           return <p>Error</p>;
 *         }
 *
 *         return <p>{data}</p>;
 *       }}
 *     </FetchNumberFacts>
 *   );
 * }
 *
 * 👉Create a render prop component that requests a random cat image from
 * https://aws.random.cat/meow
 * 👉Create a `Loading`, `Error` and `Image` component. Compose these components
 * to a `RandomCat` component that uses render props to access the requested
 * content.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎React Hooks
 * {@link https://reactjs.org/docs/hooks-intro.html}
 */
