import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 21 - HoC (Higher Order Components)
 * {@link https://reactjs.org/docs/higher-order-components.html}
 *
 * @description
 * A higher order component is a function that accepts a component and returns
 * a new React component.
 *
 * @note
 * State is difficult and complex. Therefore move state to its own component.
 * It's good practice to separate state and ui.
 *
 * @example
 * const NumberFactContainer = withNumberFacts(NumberFact);
 *
 * function withNumberFacts(WrappedComponent) {
 *   class WithNumberFacts extends React.Component {
 *     constructor(props) {
 *       super(props);
 *       this.state = {
 *         data: null,
 *         error: null,
 *         loading: false,
 *       };
 *     }
 *
 *     componentDidMount() {
 *       this.setState({ data: null, error: null, loading: true });
 *
 *       fetch('http://numbersapi.com/42')
 *         .then(response => response.text())
 *         .then(data => this.setState({ data, error: null, loading: false }))
 *         .catch(error => this.setState({ data: null, error, loading: false }));
 *     }
 *
 *     render() {
 *       return <WrappedComponent {...this.state} {...this.props} />;
 *     }
 *   }
 *
 *   return WithNumberFacts;
 * }
 *
 * function NumberFact({ data, error, loading }) {
 *   if (loading) {
 *     return <p>Loading ...</p>;
 *   }
 *
 *   if (error) {
 *     return <p>Error</p>;
 *   }
 *
 *   return <p>{data}</p>;
 * }
 *
 * @todo
 * 👉Create a hoc that requests a random cat image from https://aws.random.cat/meow
 * 👉Create a `Loading`, `Error` and `Image` component. Compose these components
 * to a `RandomCat` component that uses the hoc to access the requested content.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 🤓React Redux
 * {@link https://redux.js.org/introduction/getting-started}
 * {@link https://redux.js.org/basics/usage-with-react}
 *
 * React Redux follows the higher order pattern. It's used to `connect` your
 * components to the Redux store.
 *
 * @note Don't default to Redux. In most cases React's `useReducer`,
 * `useContext` and `createContext` suffice.
 *
 * 💎React Hooks + React Context
 * {@link https://reactjs.org/docs/hooks-reference.html#usereducer}
 * {@link https://reactjs.org/docs/hooks-reference.html#usecontext}
 * {@link https://reactjs.org/docs/context.html}
 */
