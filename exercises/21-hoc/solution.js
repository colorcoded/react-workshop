import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 21 - HoC
 */

const RandomCatContainer = withRandomCat(RandomCat);

function withRandomCat(WrappedComponent) {
  class WithRandomCat extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: null,
        error: null,
        loading: false,
      };
    }

    componentDidMount() {
      this.setState({ data: null, error: null, loading: true });

      fetch('https://aws.random.cat/meow')
        .then(response => response.json())
        .then(data => this.setState({ data, error: null, loading: false }))
        .catch(error => this.setState({ data: null, error, loading: false }));
    }

    render() {
      return <WrappedComponent {...this.state} {...this.props} />;
    }
  }
  return WithRandomCat;
}

function RandomCat({ data, error, loading }) {
  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <Error />;
  }

  if (data) {
    return <Image src={data.file} />;
  }

  return null;
}

function Loading() {
  return <p>Loading ...</p>;
}

function Error() {
  return <p>Error</p>;
}

function Image(props) {
  return <img {...props} />;
}

ReactDOM.render(<RandomCatContainer />, document.querySelector('#root'));
