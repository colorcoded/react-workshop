import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 15 - Lifecycle Methods
 * {@link http://projects.wojtekmaj.pl/react-lifecycle-methods-diagram/}
 * {@link https://reactjs.org/docs/react-component.html#the-component-lifecycle}
 *
 * @note
 * Legacy lifecycle methods:
 * {@link https://reactjs.org/docs/react-component.html#legacy-lifecycle-methods}
 * - `UNSAFE_componentWillMount()`
 * - `UNSAFE_componentWillReceiveProps()`
 * - `UNSAFE_componentWillUpdate()`
 *
 * @todo
 * 👉Run the example in the browser and check the `console.log`ged values.
 * 👉Use the React Developer Tools to change the "example" prop and/or state.
 * Check the `console.log`ged values.
 */

class Example extends React.Component {
  /**
   * @description
   * Initialized before a component is mounted. Used for initializing state and
   * binding event handlers.
   * {@link https://reactjs.org/docs/react-component.html#constructor}
   * {@link https://overreacted.io/why-do-we-write-super-props/}
   */
  constructor(props) {
    super(props); // Otherwise `this.props` will be `undefined`.
    this.state = {
      example: 'state',
    };
    console.log('constructor');
  }

  /**
   * @description
   * Invoked before every `render`. It enables a component to update internal
   * state as a result of changes in props. Should be used sparingly.
   * {@link https://reactjs.org/docs/react-component.html#static-getderivedstatefromprops}
   * {@link https://reactjs.org/blog/2018/06/07/you-probably-dont-need-derived-state.html#when-to-use-derived-state}
   */
  static getDerivedStateFromProps(props, state) {
    console.log('getDerivedStateFromProps', props, state);
    return null;
  }

  /**
   * @description
   * If `shouldComponentUpdate` returns `false`, `render` and
   * `componentDidUpdate` won't be invoked. Recommended to use the default
   * behaviour and let React re-render on every update. Use as a last resort
   * performance optimization.
   * {@link https://reactjs.org/docs/react-component.html#shouldcomponentupdate}
   */
  shouldComponentUpdate(nextProps, nextState) {
    console.log('shouldComponentUpdate', nextProps, nextState);
  }

  /**
   * @description
   * Required method. Should return a valid node (React element, Boolean, String,
   * null, ...).
   * {@link https://reactjs.org/docs/react-component.html#render}
   */
  render() {
    console.log('render');
    return null;
  }

  /**
   * @description
   * Invoked before rendering to DOM. It enables your component to capture
   * information from the DOM (e.g.: scroll position). Return value passed to
   * `componentDidUpdate`.
   * {@link https://reactjs.org/docs/react-component.html#getsnapshotbeforeupdate}
   */
  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('getSnapshotBeforeUpdate', prevProps, prevState);
    return null;
  }

  /**
   * @description
   * Invoked immediately after a component is inserted into the tree; a good
   * place to start network requests.
   * {@link https://reactjs.org/docs/react-component.html#componentdidmount}
   */
  componentDidMount() {
    console.log('componentDidMount');
  }

  /**
   * @description
   * Invoked after an update (props, state); not called after initial render.
   * {@link https://reactjs.org/docs/react-component.html#componentdidupdate}
   */
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('componentDidUpdate', prevProps, prevState, snapshot);
  }

  /**
   * @description
   * Invoked after a component unmounts; the place to do cleanup (cancel timers,
   * cancel network requests, ...).
   * {@link https://reactjs.org/docs/react-component.html#componentwillunmount}
   */
  componentWillUnmount() {
    console.log('componentWillUnmount');
  }
}

ReactDOM.render(<Example example="prop" />, document.querySelector('#root'));

/**
 * 💎React Hooks
 * {@link https://reactjs.org/docs/hooks-intro.html}
 */
