import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 23 - Context
 * {@link https://reactjs.org/docs/context.html}
 *
 * @description
 * Share data that is considered "global" to, at least, a part of the component
 * tree; e.g.: locale preference, UI theme, current authenticated use, ...
 *
 * @note
 * Used when "many components at different nesting levels" need access to the
 * same data. Avoids "prop-drilling" (passing a prop through every level of the
 * tree).
 *
 * @example
 * const ThemeContext = React.createContext(null);
 *
 * function App() {
 *   return (
 *     <ThemeContext.Provider value="light">
 *       <div>
 *         <div>
 *           <ThemeContext.Consumer>
 *             {value => <p>Current theme: {value}</p>}
 *           </ThemeContext.Consumer>
 *         </div>
 *       </div>
 *     </ThemeContext.Provider>
 *   );
 * }
 *
 * @todo
 * 👉Refactor the example `App` using a context for theme.
 * 👉Avoid prop-drilling `theme` and `user`.
 * 👉Fetch the `user` from https://api.github.com/users/stevenbenisek. Add a
 * prop to control the username.
 * 👉Add a `button` element that toggles between the light and dark theme.
 */

const theme = 'light';

const user = {
  avatarUrl: 'https://avatars3.githubusercontent.com/u/920729?v=4',
  name: 'Steven Benisek',
  html_url: 'https://github.com/stevenbenisek',
};

function App({ user, theme }) {
  return (
    <div className={`app app--${theme}`}>
      <Profile user={user} theme={theme} />
    </div>
  );
}

function Profile({ user, theme }) {
  return (
    <article className={`profile profile--${theme}`}>
      <Avatar user={user} theme={theme} />
      <Heading user={user} theme={theme} />
    </article>
  );
}

function Avatar({ user, theme }) {
  return (
    <img src={user.avatarUrl} className={`avatar avatar--${theme}`} alt="" />
  );
}

function Link({ user, theme }) {
  return (
    <a className={`link link--${theme}`} href={user.html_url}>
      {user.name}
    </a>
  );
}

function Heading({ user, theme }) {
  return (
    <h2 className={`heading heading--${theme}`}>
      <Link user={user} theme={theme} />
    </h2>
  );
}

ReactDOM.render(
  <App user={user} theme={theme} />,
  document.querySelector('#root')
);
