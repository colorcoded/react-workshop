import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 23 - Context
 *
 * @example
 * const ThemeContext = React.createContext(null);
 *
 * function useTheme() {
 *   const [theme, setTheme] = React.useState('light');
 *
 *   const toggle = React.useCallback(
 *     () => setTheme(theme === 'light' ? 'dark' : 'light'),
 *     [theme]
 *   );
 *
 *   return { theme, toggle };
 * }
 *
 * function useProfile(userName) {
 *   const [state, setState] = React.useState(null);
 *
 *   React.useEffect(() => {
 *     fetch(`https://api.github.com/users/${userName}`)
 *       .then(response => response.json())
 *       .then(data => setState(data));
 *   }, [userName]);
 *
 *   return state;
 * }
 *
 * function App(props) {
 *   const { theme, toggle } = useTheme();
 *
 *   return (
 *     <ThemeContext.Provider value={theme}>
 *       <React.Fragment>
 *         <button onClick={toggle}>Switch Theme</button>
 *         <div {...props} className={`app app--${theme}`} />
 *       </React.Fragment>
 *     </ThemeContext.Provider>
 *   );
 * }
 *
 * function Profile({ userName, ...props }) {
 *   const theme = React.useContext(ThemeContext);
 *   const user = useProfile(userName);
 *
 *   return user ? (
 *     <article {...props} className={`profile profile--${theme}`}>
 *       <Avatar src={user.avatar_url} />
 *       <Heading>
 *         <Link href={user.html_url}>{user.name}</Link>
 *       </Heading>
 *     </article>
 *   ) : null;
 * }
 *
 * function Avatar(props) {
 *   const theme = React.useContext(ThemeContext);
 *
 *   return <img {...props} className={`avatar avatar--${theme}`} alt="" />;
 * }
 *
 * function Link(props) {
 *   const theme = React.useContext(ThemeContext);
 *
 *   return <a {...props} className={`link link--${theme}`} />;
 * }
 *
 * function Heading(props) {
 *   const theme = React.useContext(ThemeContext);
 *
 *   return <h2 {...props} className={`heading heading--${theme}`} />;
 * }
 */

const ThemeContext = React.createContext(null);

class Theme extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      theme: 'light',
    };
  }

  toggle() {
    this.setState(state => ({
      theme: state.theme === 'light' ? 'dark' : 'light',
    }));
  }

  render() {
    return (
      <ThemeContext.Provider value={this.state.theme}>
        {this.props.children({
          toggle: this.toggle,
          theme: this.state.theme,
        })}
      </ThemeContext.Provider>
    );
  }
}

class FetchProfile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    };
  }

  componentDidMount() {
    fetch(`https://api.github.com/users/${this.props.userName}`)
      .then(response => response.json())
      .then(data => this.setState({ data }));
  }

  render() {
    return this.props.children(this.state);
  }
}

function App(props) {
  return (
    <Theme>
      {({ toggle, theme }) => (
        <React.Fragment>
          <button onClick={toggle}>Switch theme</button>
          <div {...props} className={`app app--${theme}`} />
        </React.Fragment>
      )}
    </Theme>
  );
}

function Profile({ userName, ...props }) {
  return (
    <ThemeContext.Consumer>
      {theme => (
        <FetchProfile userName={userName}>
          {({ data }) =>
            data ? (
              <article {...props} className={`profile profile--${theme}`}>
                <Avatar src={data.avatar_url} />
                <Heading>
                  <Link href={data.html_url}>{data.name}</Link>
                </Heading>
              </article>
            ) : null
          }
        </FetchProfile>
      )}
    </ThemeContext.Consumer>
  );
}

function Avatar(props) {
  return (
    <ThemeContext.Consumer>
      {theme => <img {...props} className={`avatar avatar--${theme}`} alt="" />}
    </ThemeContext.Consumer>
  );
}

function Link(props) {
  return (
    <ThemeContext.Consumer>
      {theme => <a {...props} className={`link link--${theme}`} />}
    </ThemeContext.Consumer>
  );
}

function Heading(props) {
  return (
    <ThemeContext.Consumer>
      {theme => <h2 {...props} className={`heading heading--${theme}`} />}
    </ThemeContext.Consumer>
  );
}

ReactDOM.render(
  <App>
    <Profile userName="kentcdodds" />
    <Profile userName="gaearon" />
    <Profile userName="mjackson" />
    <Profile userName="ryanflorence" />
  </App>,
  document.querySelector('#root')
);
