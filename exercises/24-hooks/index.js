import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 24 - Hooks
 * {@link https://reactjs.org/docs/hooks-intro.html}
 *
 * @example
 * function Example() {
 *   const [isOpen, setState] = React.useState(true);
 *   return (
 *     <React.Fragment>
 *       <button onClick={() => setState(!isOpen)}>Toggle</button>
 *       {isOpen ? <p>Hello World!!</p> : null}
 *     </React.Fragment>
 *   );
 * }
 */

class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }

  render() {
    return (
      <React.Fragment>
        <button
          onClick={() =>
            this.setState(state => ({
              isOpen: !state.isOpen,
            }))
          }
        >
          Toggle
        </button>
        {this.state.isOpen ? <p>Hello World!!</p> : null}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Example />, document.querySelector('#root'));

/**
 * 🤓React Redux
 * {@link https://redux.js.org/introduction/getting-started}
 * {@link https://redux.js.org/basics/usage-with-react}
 *
 * @note Don't default to Redux. In most cases React's `useReducer`,
 * `useContext` and `createContext` suffice.
 *
 * 💎React Hooks + React Context
 * {@link https://reactjs.org/docs/hooks-reference.html#usereducer}
 * {@link https://reactjs.org/docs/hooks-reference.html#usecontext}
 * {@link https://reactjs.org/docs/context.html}
 */
