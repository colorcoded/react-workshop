import fs from 'fs';
import path from 'path';
import cheerio from 'cheerio';
import express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import { App } from './app';

/**
 * ✨Exercise 25 - SSR
 * {@link https://reactjs.org/docs/react-dom-server.html}
 * {@link https://github.com/chentsulin/awesome-react-renderer}
 *
 * @note
 * 🕵️‍SEO: allow seach engines to crawl your pages
 * 🚀Performance: faster page loads
 * {@link https://jasonformat.com/application-holotypes/}
 *
 * @note
 * 💎Gatsby: {@link https://www.gatsbyjs.org}
 * 💎Next.js: {@link https://nextjs.org}
 *
 * @todo
 * 👉Disable JavaScript in your browser and verify that the page still works
 */

const app = express();
const port = process.env.PORT || 1234;

const clientPath = path.join(__dirname, '..', 'client');
const templatePath = path.join(clientPath, 'index.html');

app.use('/dist', express.static(clientPath));

app.get('/', function(req, res) {
  const template = cheerio.load(fs.readFileSync(templatePath));
  const markup = ReactDOM.renderToString(<App />);
  template('#root').html(markup);
  const html = template.html();
  res.send(html);
});

app.listen(port);
