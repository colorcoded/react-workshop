import React from 'react';

/**
 * ✨Exercise 25 - SSR
 * {@see ./client.js}
 * {@see ./server.js}
 */

export const App = () => <button onClick={console.log}>Hello SSR!!</button>;
