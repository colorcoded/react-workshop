import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app';

/**
 * ✨Exercise 25 - SSR
 * {@link https://reactjs.org/docs/react-dom.html#hydrate}
 */

ReactDOM.hydrate(<App />, document.querySelector('#root'));
