/**
 * ✨Exercise 01 - JavaScript
 * {@link http://kangax.github.io/compat-table/es6/}
 * {@link https://caniuse.com/#feat=es6}
 */

/**
 * 🤓Variables: `const` and `let`
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/const}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let}
 *
 * @description
 * - `var` declarations are global or function scoped, can be redeclared, the
 * value can change through reassignment
 * - `let` decalarations are block scoped, can't be redeclared, the value
 * can change through reassignment
 * - `const` declarations are block scoped, can't be redeclared, the value
 * can't change through reassignment
 *
 * @todo
 * 👉Rewrite the `var` declarations with `const` and `let` and compare the
 * logged values.
 */

if (true) {
  var x = 1;
}
console.log('Variables - Scope:', x);

var y = 0;
var y = 1;
console.log('Variables - Redeclaration:', y);

var z = 0;
z = 1;
console.log('Variables - Reassignment:', z);

/**
 * 🤓Template literals
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#Expression_interpolation}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals#Multi-line_strings}
 *
 * @description
 * Template literals are string lirerals allowing string interpolations and
 * multi-line strings
 *
 * @example
 * const count = 1;
 * console.log(`count: ${count}`); // "count: 1"
 *
 * @todo
 * 👉Use template literals to log "Hello World!!"
 * 👉Log "Hello" and "World!!" on a separate line
 */

const world = 'World';
console.log('Template literals:', 'Hello ' + world + '!!');

/**
 * 🤓Arrow Functions
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions#No_separate_this}
 *
 * @example
 * const sum = (a, b) => a + b;
 * console.log(sum(1, 2)); // 3
 *
 * @todo
 * 👉Rewrite `last` as an arrow function.
 * 👉Create a plain JavaScript object. Name it however you like. Add two methods,
 * a regular function and an arrow function. Both functions should execute
 * `console.log(this)`. Run the functions and compare the logged values.
 */

function last(array) {
  return array[array.length - 1];
}
console.log('Arrow Functions:', last([1, 2, 3]));

/**
 * 🤓Default Parameters
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters}
 *
 * @example
 * const multiply = (a, b = 1) => a * b;
 * console.log(multiply(2)); // 2
 * console.log(multiply(2, 2)); // 4
 *
 * @todo
 * 👉Use default parameters to log "Hello World!!" twice.
 */

function greet(name) {
  return `Hello ${name}!!`;
}
console.log('Default Parameters:', greet('World'), greet());

/**
 * 🤓Property Shorthand
 *
 * @example
 * const name = 'Steven';
 * const person = { name };
 * console.log(person); // { name: Steven }
 *
 * @todo
 * 👉Use the property shorthand syntax to add `bar` to `foo`.
 */

const bar = 'bar';
const foo = { bar: bar };
console.log('Property Shorthand:', foo.bar);

/**
 * 🤓Object Destructuring
 * {@link https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Operatoren/Destructuring_assignment#Object_destructuring}
 *
 * @note
 * Syntax to extract data from objects into variables
 *
 * @example
 * const person = {
 *   age: 35,
 *   first: 'Steven',
 *   last: 'Benisek',
 * };
 * const { age, first, last } = person;
 * console.log(age); // 35
 * console.log(first); // Steven
 * console.log(last); // Benisek
 *
 * @todo
 * 👉Use object destructuring to get the person's full name.
 */

function getFullName(person) {
  return `${person.firstName} ${person.lastName}`;
}
console.log(
  'Object Destructuring:',
  getFullName({ firstName: 'Steven', lastName: 'Benisek' })
);

/**
 * 🤓Array Destructuring
 * {@link https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Operatoren/Destructuring_assignment#Array_destructuring}
 *
 * @note
 * Syntax to extract data from arrays into variables
 *
 * @example
 * const person = ['Steven', 'Benisek'];
 * const [first, last] = person;
 * console.log(first); // Steven
 * console.log(last); // Benisek
 *
 * @todo
 * 👉Use array destructuring to return the array's first item.
 */

function first(array) {
  return array[0];
}
console.log('Array Destructuring:', first([1, 2, 3]));

/**
 * 🤓Spread Operator
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#Spread_properties}
 *
 * @note
 * Spread properties to an object literal. Copy properties from an object to
 * a new object.
 *
 * @example
 * const age = 35;
 * const name = {
 *   first: 'Steven',
 *   last: 'Benisek',
 * };
 * const person = { age, ...name };
 * console.log(person); // { age: 35, first: Steven, last: Benisek }
 *
 * @todo
 * 👉Use the spread operator to rewrite the `clone` function. The logged value
 * should remain the same.
 */

const o = { x: 1, y: 2, z: 3 };
function clone(object) {
  return Object.assign({}, object);
}
console.log('Spread Operator:', o === clone(o));

/**
 * 🤓Rest Syntax
 *
 * @note
 * Only the last parameter can be a rest parameter
 *
 * @example
 * const person = {
 *   age: 35,
 *   first: 'Steven',
 *   last: 'Benisek',
 * };
 * const { age, ...rest } = person;
 * console.log(age); // 35
 * console.log(rest); // { first: Steven, last: Benisek }
 *
 * @todo
 * 👉Use the rest syntax to log all the arguments.
 */

function log(a, b) {
  console.log('Rest Syntax:', a, b);
}
log(1, 2, 3, 4, 5, 6, 7, 8);

/**
 * 🤓Array.prototype.map
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map}
 *
 * @example
 * const numbers = [1, 2, 3, 4];
 * const result = numbers.map(number => number * 2);
 * console.log(result); // [2, 4, 6, 8]
 *
 * @todo
 * 👉Use `.map` to log each item as a string; e.g.: "Item 1", "Item 2", ...
 */

console.log('Array.prototype.map:', [1, 2, 3, 4]);

/**
 * 🤓ES Modules
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export}
 * {@link https://jakearchibald.com/2017/es-modules-in-browsers/}
 * {@link http://2ality.com/2018/12/nodejs-esm-phases.html}
 *
 * @example
 * import React from 'react';
 * console.log(React.createElement);
 *
 * @example
 * import { createElement } from 'react';
 * console.log(createElement);
 */

/**
 * 🤓Fetch
 * {@link https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch}
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise}
 * {@link https://caniuse.com/#feat=fetch}
 * {@link https://github.com/github/fetch}
 *
 * @example
 * fetch('https://domain.com/json')
 *  .then(response => response.json())
 *  .then(data => console.log(data));
 *
 * @example
 * fetch('https://domain.com/text')
 *  .then(response => response.text())
 *  .then(data => console.log(data));
 *
 * @todo
 * 👉Fetch and log the data from http://numbersapi.com/42
 */

fetch('https://aws.random.cat/meow')
  .then(response => response.json())
  .then(data => console.log('fetch', data));

/**
 * 🚀Babel
 * {@link https://babeljs.io/repl}
 * {@link https://babeljs.io/en/setup/}
 * {@link https://babeljs.io/docs/en/babel-preset-env}
 */

/**
 * 📝Parcel
 * {@link https://parceljs.org/javascript.html#babel}
 */
