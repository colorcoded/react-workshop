/**
 * ✨Exercise 01 - JavaScript
 */

/**
 * 🤓Template literals
 */

const world = 'World';
console.log('Template literals:', `Hello ${world}!!`);

/**
 * 🤓Arrow Functions
 *
 * @example
 * const last = array => {
 *   return array[array.length - 1];
 * }
 */

const last = array => array[array.length - 1];
console.log('Arrow Functions:', last([1, 2, 3]));

/**
 * 🤓Default Parameters
 */

function greet(name = 'World') {
  return `Hello ${name}!!`;
}
console.log('Default Parameters:', greet('World'), greet());

/**
 * 🤓Property Shorthand
 */

const bar = 'bar';
const foo = { bar };
console.log('Property Shorthand:', foo.bar);

/**
 * 🤓Object Destructuring
 *
 * @example
 * const getFullName = ({ firstName, lastName }) => `${firstName} ${lastName}`;
 */

function getFullName({ firstName, lastName }) {
  return `${firstName} ${lastName}`;
}
console.log(
  'Object Destructuring:',
  getFullName({ firstName: 'Steven', lastName: 'Benisek' })
);

/**
 * 🤓Array Destructuring
 *
 * @example
 * const first = ([first]) => first;
 */

function first([first]) {
  return first;
}
console.log('Array Destructuring:', first([1, 2, 3]));

/**
 * 🤓Spread Operator
 *
 * @example
 * const clone = object => ({...object});
 */

const o = { x: 1, y: 2, z: 3 };
function clone(object) {
  return { ...object };
}
console.log('Spread Operator:', o === clone(o));

/**
 * 🤓Rest Syntax
 *
 * @example
 * const log = (a, b, ...rest) => {
 *   console.log('Rest Syntax:', a, b, ...rest);
 * }
 *
 * @example
 * function log(...args) {
 *   console.log('Rest Syntax:', ...args);
 * }
 *
 * @example
 * const log = (...args) => {
 *   console.log('Rest Syntax:', ...args);
 * }
 */

function log(a, b, ...rest) {
  console.log('Rest Syntax:', a, b, ...rest);
}
log(1, 2, 3, 4, 5, 6, 7, 8);

/**
 * 🤓Array.prototype.map
 */

console.log('Array.prototype.map:', [1, 2, 3, 4].map(item => `Item ${item}`));

/**
 * 🤓Fetch
 */

fetch('http://numbersapi.com/42')
  .then(response => response.text())
  .then(data => console.log('fetch', data));
