import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 17 - State
 * {@link https://reactjs.org/docs/react-component.html#setstate}
 *
 * @description
 * "`setState()` enqueues changes to the component state and tells React that
 * this component and its children need to be re-rendered with the updated state."
 *
 * @note
 * `setState` requests can be deferred and batched
 *
 * @example
 * class Example extends React.Component {
 *   constructor(props) {
 *     super(props);
 *     this.handleClick = this.handleClick.bind(this);
 *     this.state = {
 *       count: 0,
 *     };
 *   }
 *
 *   handleClick() {
 *     function updater(state, props) {
 *       console.log(state); // 0
 *       return { count: state.count + 1 };
 *     }
 *
 *     function callback() {
 *       console.log(this.state); // 1
 *     }
 *
 *     this.setState(updater, callback);
 *
 *     console.log(this.state); // 0
 *   }
 *
 *   render() {
 *     return (
 *       <div>
 *         <p>You clicked {this.state.count} times</p>
 *         <button onClick={this.handleClick}>Click Me!!</button>
 *       </div>
 *     );
 *   }
 * }
 *
 * @todo
 * 👉Create a counter component. It should display the current value and have a
 * button to increment and a button to decrement that value.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎React Hooks
 * {@link https://reactjs.org/docs/hooks-intro.html}
 */
