import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 17 - State
 *
 * @example
 * function Counter() {
 *   const [count, setCount] = React.useState(0);
 *   const handleDecrement = React.useCallback(() => setCount(count - 1));
 *   const handleIncrement = React.useCallback(() => setCount(count + 1));
 *
 *   return (
 *     <div>
 *       <p>Count: {count}</p>
 *       <button onClick={handleDecrement}>-</button>
 *       <button onClick={handleIncrement}>+</button>
 *     </div>
 *   );
 * }
 */

class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.handleIncrement = this.handleIncrement.bind(this);
    this.handleDecrement = this.handleDecrement.bind(this);
    this.state = {
      count: 0,
    };
  }

  handleDecrement() {
    this.setState(currentState => ({
      count: currentState.count - 1,
    }));
  }

  handleIncrement() {
    this.setState(currentState => ({
      count: currentState.count + 1,
    }));
  }

  render() {
    return (
      <div>
        <p>Count: {this.state.count}</p>
        <button onClick={this.handleDecrement}>-</button>
        <button onClick={this.handleIncrement}>+</button>
      </div>
    );
  }
}

ReactDOM.render(<Counter />, document.querySelector('#root'));
