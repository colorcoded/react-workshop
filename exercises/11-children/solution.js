import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 11 - Children
 */

function Grid(props) {
  return <div {...props} className="grid" />;
}

ReactDOM.render(
  <Grid>
    <span>Item 1</span>
    <span>Item 2</span>
    <span>Item 3</span>
    <span>Item 4</span>
    <span>Item 5</span>
    <span>Item 6</span>
    Item 7
  </Grid>,
  document.querySelector('#root')
);
