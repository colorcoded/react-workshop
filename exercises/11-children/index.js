import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 11 - Children
 * {@link https://sid.studio/post/just-use-children/}
 * {@link https://mxstbr.blog/2017/02/react-children-deepdive/}
 *
 * @example
 * function Example(props) {
 *   return <div>{props.children}</div>
 * }
 *
 * @example
 * function Example(props) {
 *   return <div children={props.children} />;
 * }
 *
 * @todo
 * 👉Create a `Grid` component that renders a `div` element, is styled using
 * the provided `grid` CSS class and accepts `children`.
 * 👉Children can be anything: components, strings, arrays, functions, .... Give
 * it a go!
 */

ReactDOM.render(null, document.querySelector('#root'));
