/**
 * ✨Exercise 04 - Hello World
 */

const p = React.createElement('p', { id: 'hello-world' }, 'Hello World!!');

console.log(p);

ReactDOM.render(p, document.querySelector('#root'));
