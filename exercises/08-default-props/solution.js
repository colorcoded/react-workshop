import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 08 - defaultProps
 *
 * @example
 * function Greet({ firstName = 'John', lastName = 'Doe', ...props }) {
 *   return (
 *     <p {...props}>
 *       Hello, {firstName} {lastName}!!
 *     </p>
 *   );
 * }
 */

function Greet({ firstName, lastName, ...props }) {
  return (
    <p {...props}>
      Hello, {firstName} {lastName}!!
    </p>
  );
}

Greet.defaultProps = {
  firstName: 'John',
  lastName: 'Doe',
};

ReactDOM.render(<Greet />, document.querySelector('#root'));
