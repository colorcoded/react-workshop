import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 08 - defaultProps
 *
 * @example
 * function Example(props) {
 *  return <div>Example: {props.example}</div>;
 * }
 *
 * Example.defaultProps = {
 *   example: 'example',
 * };
 *
 * @todo
 * 👉Create a component that accepts two properties: `firstName` and `lastName`.
 * Assuming the first name is "John" and last name is "Doe" it should render a
 * `p` element with the text "Hello, John Doe!!".
 * 👉Add `defaultProps` for both properties.
 * 👉Update your component and use default parameters instead of `defaultProps`.
 */

ReactDOM.render(null, document.querySelector('#root'));
