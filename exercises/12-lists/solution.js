import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 12 - List
 */

const galaxies = [
  'Andromeda',
  'Black Eye Galaxy',
  "Bode's Galaxy",
  'Cartwheel Galaxy',
  'Cigar Galaxy',
  'Comet Galaxy',
  'Cosmos Redshift 7',
  "Hoag's Object",
];

function List({ items }) {
  return (
    <ul>
      {items.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>
  );
}

ReactDOM.render(<List items={galaxies} />, document.querySelector('#root'));
