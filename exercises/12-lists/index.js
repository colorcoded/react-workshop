import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 12 - List
 * {@link https://reactjs.org/docs/lists-and-keys.html}
 *
 * @todo
 * 👉Create a `List` component that renders a `ul` with nested `li` elements.
 * Use this component to display the list of provided galaxies. Remember that
 * arrays are valid `children`. TIP: use `Array.prototype.map`.
 * 👉Add an `items` prop that allows you to pass the list of galaxies via props.
 * 👉Try rendering multiple adjacent `List` components; eg:
 * `<List>...</List><List>...</List>`
 */

const galaxies = [
  'Andromeda',
  'Black Eye Galaxy',
  "Bode's Galaxy",
  'Cartwheel Galaxy',
  'Cigar Galaxy',
  'Comet Galaxy',
  'Cosmos Redshift 7',
  "Hoag's Object",
];

ReactDOM.render(null, document.querySelector('#root'));
