import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 16 - Event Handlers
 *
 * @example
 * class Button extends React.Component {
 *   handleClick(event) {
 *     console.log(event);
 *   }
 *
 *   render() {
 *     return <button onClick={this.handleClick}>{this.props.children}</button>;
 *   }
 * }
 */

function Button(props) {
  return <button {...props} />;
}

ReactDOM.render(
  <Button onClick={event => console.log(event)}>Log event</Button>,
  document.querySelector('#root')
);
