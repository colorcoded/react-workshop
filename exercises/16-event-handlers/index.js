import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 16 - Event Handlers
 * {@link https://reactjs.org/docs/handling-events.html}
 * {@link https://reactjs.org/docs/events.html#supported-events}
 *
 * @description
 * Similar to DOM element events, but:
 * - Use camelCase, instead of lowercase; e.g.: `onClick`
 * - Pass a function instead of a string
 * - Cross-browser
 *
 * @example
 * function Example() {
 *   return <button onClick={() => console.log('example')}>Example</button>
 * }
 *
 * @todo
 * 👉Create a component that logs its `onClick` event to the console.
 * 👉Update the component and pass the `onClick` event and text via props. TIP:
 * use the `Spread Operator` syntax.
 * 👉Rewrite your functional component as a class component and use a class
 * method to handle the event.
 */

ReactDOM.render(null, document.querySelector('#root'));
