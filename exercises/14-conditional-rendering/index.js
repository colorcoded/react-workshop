import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 14 - Conditional Rendering
 * {@link https://reactjs.org/docs/conditional-rendering.html}
 * {@link https://www.robinwieruch.de/conditional-rendering-react/}
 *
 * @note
 * You can use:
 * - ternary operator
 * - if/else
 * - switch/case
 *
 * @example
 * function Example(props) {
 *   return props.isVisible ? <p>Example</p> : null
 * }
 *
 * @todo
 * 👉Create a `List` component that renders a `ul` with nested `li` elements.
 * Use this component to display a list of items controlled via props.
 * 👉Update your component so it doesn't throw a `TypeError` when passing `null`
 * as a value for `props.items`. Use the ternary operator or an if/else-statement
 * to conditionally render a different output.
 * 👉Render a message if the list of items is empty.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 🤓Ternary operator
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator}
 *
 * @example
 * const result = true ? 'true' : 'false';
 * console.log(result); // 'true'
 */
