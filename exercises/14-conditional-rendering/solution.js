import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 14 - Conditional Rendering
 */

function List({ items }) {
  if (!items) {
    return null;
  }

  if (!items.length) {
    return <p>The list is empty!!</p>;
  }

  return (
    <ul>
      {items.map(item => (
        <li key={item}>{item}</li>
      ))}
    </ul>
  );
}

ReactDOM.render(
  <React.Fragment>
    <List />
    <List items={[]} />
    <List items={[1, 2, 3]} />
  </React.Fragment>,
  document.querySelector('#root')
);
