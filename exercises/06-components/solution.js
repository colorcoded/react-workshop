import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 06 - Components
 */

function Greet() {
  return <p>Hello, World!!</p>;
}

ReactDOM.render(<Greet />, document.querySelector('#root'));
