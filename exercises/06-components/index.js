import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 06 - Components
 * {@link https://reactjs.org/docs/components-and-props.html}
 *
 * @note
 * Two types of components:
 * - functional components
 * - class components
 *
 * @example
 * function Example() {
 *  return <div>Example</div>;
 *  // return React.createElement('div', null, 'Example');
 * }
 *
 * @example
 * class Example extends React.Component {
 *  render() {
 *    return <div>Example</div>;
 *    // return React.createElement('div', null, 'Example');
 *  }
 * }
 *
 * @todo
 * 👉Create a component that renders a `p` element with the text "Hello, World!!".
 */

ReactDOM.render(null, document.querySelector('#root'));
