/**
 * ✨Exercise 02 - Imperative vs Declarative
 */

const root = document.querySelector('#root');

function updateImperative() {
  const list = root.querySelector('ul');
  const listItems = list.querySelectorAll('li');
  const firstListItem = listItems[0];
  firstListItem.classList.add('first');
  const newListItem = document.createElement('li');
  newListItem.classList.add('last');
  newListItem.innerText = 'Item 5';
  list.appendChild(newListItem);
}

function updateDeclarative() {
  root.innerHTML = `
    <ul>
      <li class="first">Item 1</li>
      <li>Item 2</li>
      <li>Item 3</li>
      <li>Item 4</li>
      <li class="last">Item 5</li>
    </ul>
  `;
}

// setTimeout(updateImperative, 5 * 1000)
setTimeout(updateDeclarative, 5 * 1000);
