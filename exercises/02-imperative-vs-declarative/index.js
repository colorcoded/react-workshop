/**
 * ✨Exercise 02 - Imperative vs Declarative
 *
 * @description
 * "Imperative programming is a programming paradigm that uses statements
 * that change a program’s state"
 * {@link https://en.wikipedia.org/wiki/Imperative_programming}
 *
 * @description
 * "Declarative programming is a programming paradigm … that expresses the
 * logic of a computation without describing its control flow."
 * {@link https://en.wikipedia.org/wiki/Declarative_programming}
 *
 * @example
 * <ul>
 *   <li class="first">Item 1</li>
 *   <li>Item 2</li>
 *   <li>Item 3</li>
 *   <li>Item 4</li>
 *   <li class="last">Item 5</li>
 * </ul>
 *
 * @todo
 * 👉Use JavaScript to select the first list item and add the className
 * "first".
 * 👉Use JavaScript to create a fifth list item. Set the text to "Item 5",
 * add the className "last" and append it to the list.
 *
 * @note
 * "The DOM is an object based representation of an HTML document and an
 * interface to manipulate that object"
 * {@link https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model}
 *
 * @example Initial DOM 🌲
 * html
 *   head
 *   body
 *     h1
 *       Exercise 02 - Imperative vs Declarative
 *     div id="root"
 *       ul
 *         li
 *           Item 1
 *         li
 *           Item 2
 *         li
 *           Item 3
 *         li
 *           Item 4
 *
 * @example Final DOM 🌲
 * html
 *   head
 *   body
 *     h1
 *       Exercise 02 - Imperative vs Declarative
 *     div id="root"
 *       ul
 *         li class="first"
 *           Item 1
 *         li
 *           Item 2
 *         li
 *           Item 3
 *         li
 *           Item 4
 *         li class="last"
 *           Item 5
 *
 * @todo
 * 👉Use JavaScript to replace the innerHTML of `div#root` with the HTML
 * of our final list. (Don't remove your previous solution!! You'll need
 * it later on in the exercise. Use comments to disable it.) TIP: use `Template
 * Literals`.
 *
 * @example
 * const html = `
 *   <ul>
 *     <li>Item 1</li>
 *     <li>Item 2</li>
 *   </ul>
 * `;
 *
 * @todo
 * 👉Wrap your logic in a function and execute the code after a timeout of
 * x seconds. Open your browser's developer tools. When the update happens
 * you will see the specific elements that change.
 * 👉Repeat this using your initial solution.
 *
 *
 * @example
 * function update() {
 *  // your code here
 * }
 * setTimeout(update, 5 * 1000);
 *
 * @note
 * 🚀Degraded performance: manipulating the DOM can be expensive
 * 👂Loss of event listeners; e.g.: `onClick` attached to a `<button>`
 * 👀Loss of state; e.g: `focus` state of an `<input>` element
 */
