import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/**
 * ✨Exercise 09 - PropTypes
 * {@link https://github.com/facebook/prop-types}
 *
 * @example
 * function Example(props) {
 *   return <div>Example: {props.example}</div>;
 * }
 *
 * Hello.propTypes = {
 *   example: PropTypes.string.isRequired,
 * };
 *
 * @todo
 * 👉Create a component that accepts two properties: `firstName` and `lastName`.
 * Assuming the first name is "John" and last name is "Doe" it should render a
 * `p` element with the text "Hello, John Doe!!".
 * 👉Add `propTypes` for both properties.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎Static Type Checking with TypeScript
 * {@link https://reactjs.org/docs/static-type-checking.html#typescript}
 */
