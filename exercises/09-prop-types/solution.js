import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/**
 * ✨Exercise 09 - PropTypes
 */

function Greet({ firstName, lastName, ...props }) {
  return (
    <p {...props}>
      Hello, {firstName} {lastName}!!
    </p>
  );
}

Greet.defaultProps = {
  firstName: 'John',
  lastName: 'Doe',
};

Greet.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
};

ReactDOM.render(<Greet />, document.querySelector('#root'));
