import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 20 - Refs
 *
 * @example
 * function Button(props) {
 *   const buttonRef = React.useRef();
 *
 *   React.useEffect(() => {
 *     buttonRef.current.focus();
 *   }, []);
 *
 *   return <button ref={buttonRef}>{props.children}</button>;
 * }
 */

class Button extends React.Component {
  constructor(props) {
    super(props);
    this.buttonRef = React.createRef();
  }

  componentDidMount() {
    this.buttonRef.current.focus();
  }

  render() {
    return <button ref={this.buttonRef}>{this.props.children}</button>;
  }
}

ReactDOM.render(<Button>Button</Button>, document.querySelector('#root'));
