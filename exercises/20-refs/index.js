import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 20 - Refs
 * {@link https://reactjs.org/docs/refs-and-the-dom.html}
 * {@link https://reactjs.org/docs/integrating-with-other-libraries.html}
 * {@link https://reactjs.org/docs/uncontrolled-components.html}
 *
 * @description
 * Store a reference to a DOM node and access the element
 *
 * @note
 * - Manage focus
 * - Uncontrolled form components
 * - Integrate with 3rd party libraries; e.g.: jQuery plugins
 * - Animations
 *
 * @example
 * class Example extends React.Component {
 *   constructor(props) {
 *     super(props);
 *     // 1. Create a ref
 *     this.divRef = React.createRef();
 *   }
 *
 *   componentDidMount() {
 *     // 3. Access the ref
 *     console.log(this.divRef.current);
 *   }
 *
 *   render() {
 *     // 2. Add the ref to a DOM node
 *     return <div ref={this.divRef}>{this.props.children}</div>;
 *   }
 * }
 *
 * @todo
 * 👉Create a component that moves focus to a `button` element when it mounts.
 */

ReactDOM.render(null, document.querySelector('#root'));
