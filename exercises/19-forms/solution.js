import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 19 - Forms
 */

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { name: '', email: '', error: null };
  }

  handleNameChange(event) {
    this.setState({
      name: event.target.value,
    });
  }

  handleEmailChange(event) {
    const value = event.target.value;
    let error = null;

    if (!value.includes('@')) {
      error = new Error('Invalid email address');
    }

    this.setState({
      email: value,
      error,
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    if (this.state.error) {
      console.error(this.state.error);
      return;
    }

    window.localStorage.setItem(
      'react-workshop-form',
      JSON.stringify({
        name: this.state.name,
        email: this.state.email,
      })
    );
  }

  componentDidMount() {
    const localState = window.localStorage.getItem('react-workshop-form');
    this.setState(JSON.parse(localState));
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} noValidate>
        <label htmlFor="name">Name</label>
        <input
          id="name"
          type="text"
          value={this.state.name}
          onChange={this.handleNameChange}
        />
        <label htmlFor="email">E-mail</label>
        <input
          id="email"
          type="email"
          value={this.state.email}
          onChange={this.handleEmailChange}
        />
        <button type="submit">Submit</button>
      </form>
    );
  }
}

ReactDOM.render(<Form />, document.querySelector('#root'));
