import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 19 - Forms
 * {@link https://reactjs.org/docs/forms}
 *
 * @note
 * HTML form elements (`<input>`, `<textarea>`, `<select>`, ...) maintain their
 * own state. Controlled form components use `onChange` handlers to store state
 * in React and have React control the form element's values. Form data is
 * handled by React instead of the DOM.
 *
 * @example
 * class Example extends React.Component {
 *   constructor(props) {
 *     super(props);
 *     this.state = { value: '' };
 *     this.handleChange = this.handleChange.bind(this);
 *     this.handleSubmit = this.handleSubmit.bind(this);
 *   }
 *
 *   handleChange(event) {
 *     this.setState({
 *       value: event.target.value,
 *     });
 *   }
 *
 *   handleSubmit(event) {
 *     console.log(this.state.value);
 *     event.preventDefault();
 *   }
 *
 *   render() {
 *     return (
 *       <form onSubmit={this.handleSubmit}>
 *         <label htmlFor="name">Name</label>
 *         <input
 *           id="name"
 *           type="text"
 *           value={this.state.value}
 *           onChange={this.handleChange}
 *         />
 *         <button type="submit">Submit</button>
 *       </form>
 *     );
 *   }
 * }
 *
 * @todo
 * 👉Create a form component with a `name` and `email` field. Keep track
 * of the values and `console.log` as the user types.
 * 👉Render an error message if the `email` field doesn't contain an "@" symbol
 * and stop the form from being submitted.
 * 👉Save the form's state and restore on page refresh.
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎Formik
 * {@link https://github.com/jaredpalmer/formik}
 */

/**
 * 💎Uncontrolled Components
 * {@link https://reactjs.org/docs/uncontrolled-components}
 */
