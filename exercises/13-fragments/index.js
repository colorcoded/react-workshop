import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 13 - Fragments
 * {@link https://reactjs.org/docs/fragments.html}
 *
 * @description
 * Return a list of elements from a component without adding extra DOM nodes.
 * Adding an extra DOM node would invalidate HTML or break CSS; e.g.:
 * - `<tr><td>Item</td></tr>`
 * - `<ul><li>Item</li></ul>`
 * - `<div style="display: flex;"><div style="flex: 1 1 auto;">Item</div></div>`
 *
 * @example
 * function Example() {
 *   return (
 *     <React.Fragment>
 *       <ExampleChild />
 *       <ExampleChild />
 *       <ExampleChild />
 *     </React.Fragment>
 *   )
 * }
 *
 * @todo
 * 👉Create a `List` component that renders a `ul` with nested `li` elements.
 * Use this component to display the list of provided galaxies.
 * 👉Seperate the `li` elements into a custom `ListItems` components. Use
 * `React.Fragment` to return the items.
 */

const galaxies = [
  'Andromeda',
  'Black Eye Galaxy',
  "Bode's Galaxy",
  'Cartwheel Galaxy',
  'Cigar Galaxy',
  'Comet Galaxy',
  'Cosmos Redshift 7',
  "Hoag's Object",
];

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 🤓DocumentFragment
 * {@link https://developer.mozilla.org/en-US/docs/Web/API/Document/createDocumentFragment}
 *
 * @description
 * DocumentFragment's are DOM nodes, but not part of the DOM tree
 *
 * @example
 * const fragment = document.createDocumentFragment();
 * galaxies.forEach(galaxy => {
 *   const li = document.createElement('li');
 *   li.textContent = galaxy;
 *   fragment.appendChild(li);
 * });
 *
 * @todo
 * 👉 Run the example in the browser and `console.log(fragment)`.
 */
