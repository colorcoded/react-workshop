import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 13 - Fragments
 */

const galaxies = [
  'Andromeda',
  'Black Eye Galaxy',
  "Bode's Galaxy",
  'Cartwheel Galaxy',
  'Cigar Galaxy',
  'Comet Galaxy',
  'Cosmos Redshift 7',
  "Hoag's Object",
];

function List(props) {
  return <ul {...props} />;
}

function ListItems({ items }) {
  return (
    <React.Fragment>
      {items.map(item => (
        <li key={item}>{item}</li>
      ))}
    </React.Fragment>
  );
}

ReactDOM.render(
  <List>
    <ListItems items={galaxies} />
  </List>,
  document.querySelector('#root')
);
