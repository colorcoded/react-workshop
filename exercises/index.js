import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <ol>
    {[
      'javascript',
      'imperative-vs-declarative',
      'virtual-dom',
      'hello-world',
      'jsx',
      'components',
      'props',
      'default-props',
      'prop-types',
      'styling',
      'children',
      'lists',
      'fragments',
      'conditional-rendering',
      'lifecycle-methods',
      'event-handlers',
      'state',
      'data-fetching',
      'forms',
      'refs',
      'hoc',
      'render-prop',
      'context',
      'hooks',
    ].map((item, index) => (
      <li key={item}>
        <a href={`${String(index + 1).padStart(2, 0)}-${item}/index.html`}>
          {item}
        </a>
      </li>
    ))}
  </ol>,
  document.querySelector('#root')
);
