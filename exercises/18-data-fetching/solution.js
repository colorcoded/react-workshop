import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 18 - Data Fetching
 *
 * @example
 * function RandomCat() {
 *   const [data, setData] = React.useState(null);
 *   const [error, setError] = React.useState(null);
 *   const [isLoading, setIsLoading] = React.useState(false);
 *
 *   React.useEffect(() => {
 *     setIsLoading(true);
 *
 *     fetch('https://aws.random.cat/meow')
 *       .then(response => response.json())
 *       .then(data => {
 *         setIsLoading(false);
 *         setData(data);
 *       })
 *       .catch(error => {
 *         setIsLoading(false);
 *         setError(error);
 *       });
 *   }, []);
 *
 *   if (isLoading) {
 *     return <p>Loading ...</p>;
 *   }
 *
 *   if (error) {
 *     return (
 *       <React.Fragment>
 *         <p>Error</p>
 *         <pre>{JSON.stringify(error.message, null, 2)}</pre>
 *       </React.Fragment>
 *     );
 *   }
 *
 *   if (data) {
 *     return <img src={data.file} />;
 *   }
 *
 *   return null;
 * }
 *
 * @example
 * const initialState = {
 *   error: null,
 *   data: null,
 *   isLoading: false,
 * };
 *
 * function reducer(state, action) {
 *   switch (action.type) {
 *     case 'init':
 *       return {
 *         error: null,
 *         data: null,
 *         isLoading: true,
 *       };
 *     case 'success':
 *       return {
 *         error: null,
 *         data: action.payload,
 *         isLoading: false,
 *       };
 *     case 'fail':
 *       return {
 *         error: action.payload,
 *         data: null,
 *         isLoading: false,
 *       };
 *     default:
 *       throw new Error();
 *   }
 * }
 *
 * function RandomCat() {
 *   const [state, dispatch] = React.useReducer(reducer, initialState);
 *
 *   React.useEffect(() => {
 *     dispatch({ type: 'init' });
 *
 *     fetch('https://aws.random.cat/meow')
 *       .then(response => response.json())
 *       .then(data => dispatch({ type: 'success', payload: data }))
 *       .catch(error => dispatch({ type: 'fail', payload: error }));
 *   }, []);
 *
 *   if (state.isLoading) {
 *     return <p>Loading ...</p>;
 *   }
 *
 *   if (state.error) {
 *     return (
 *       <React.Fragment>
 *         <p>Error</p>
 *         <pre>{JSON.stringify(state.error.message, null, 2)}</pre>
 *       </React.Fragment>
 *     );
 *   }
 *
 *   if (state.data) {
 *     return <img src={state.data.file} />;
 *   }
 *
 *   return null;
 * }
 */

class RandomCat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      error: null,
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({
      data: null,
      error: null,
      isLoading: true,
    });

    fetch('https://aws.random.cat/meow')
      .then(response => response.json())
      .then(data =>
        this.setState({
          data,
          error: null,
          isLoading: false,
        })
      )
      .catch(error =>
        this.setState({
          data: null,
          error,
          isLoading: false,
        })
      );
  }

  render() {
    if (this.state.isLoading) {
      return <p>Loading ...</p>;
    }

    if (this.state.error) {
      return (
        <React.Fragment>
          <p>Error</p>
          <pre>{JSON.stringify(this.state.error.message, null, 2)}</pre>
        </React.Fragment>
      );
    }

    if (this.state.data) {
      return <img src={this.state.data.file} />;
    }

    return null;
  }
}

ReactDOM.render(<RandomCat />, document.querySelector('#root'));
