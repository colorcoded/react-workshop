import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 18 - Data Fetching
 *
 * @note
 * React will re-render on every state change. Therefore it's best to fetch
 * remote data from `componentDidMount`. This method will run only once in a
 * component's lifecycle. Doing so prevents the request from happening every
 * `render`.
 *
 * @example
 * class Example extends React.Component {
 *   constructor(props) {
 *     super(props);
 *     this.state = {
 *       data: null,
 *     };
 *   }
 *
 *   componentDidMount() {
 *     fetch('http://numbersapi.com/42')
 *       .then(response => response.text())
 *       .then(data => this.setState({ data }));
 *   }
 *
 *   render() {
 *     return this.state.data ? <p>{this.state.data}</p> : null;
 *   }
 * }
 *
 * @todo
 * 👉Create a component that displays a random cat image from
 * https://aws.random.cat/meow
 * 👉Display a `p` element with the text "loading ..." untill the data has
 * loaded.
 * 👉Display a `p` element with the text "error" if the request fails. Add the
 * error message to `p`'s text; e.g.: `JSON.stringify(error, null, 2)`
 */

ReactDOM.render(null, document.querySelector('#root'));

/**
 * 💎Async/await
 * {@link https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function}
 *
 * @note
 * 🚀Use {@link https://github.com/MatAtBread/fast-async} instead of
 * {@link https://babeljs.io/docs/en/babel-plugin-transform-regenerator}
 *
 * @example
 * import 'babel-polyfill';
 * (async function() {
 *   const response = await fetch('http://numbersapi.com/42');
 *   const data = await response.text();
 *   console.log(data);
 * })();
 *
 * @todo
 * 👉 Rewrite `componentDidMount` as an async function and await the response.
 */
