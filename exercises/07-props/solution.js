import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 07 - Props
 */

function Greet({ firstName, lastName, ...props }) {
  return (
    <p {...props}>
      Hello, {firstName} {lastName}!!
    </p>
  );
}

ReactDOM.render(
  <Greet firstName="John" lastName="Doe" id="greet" />,
  document.querySelector('#root')
);
