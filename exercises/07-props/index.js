import React from 'react';
import ReactDOM from 'react-dom';

/**
 * ✨Exercise 07 - Props
 * {@link https://reactjs.org/docs/components-and-props.html}
 *
 * @example
 * function Example(props) {
 *  return <div>Example: {props.example}</div>;
 * }
 *
 * @example
 * function Example({ example, ...rest }) {
 *  return <div {...rest}>Example: {example}</div>;
 * }
 *
 * @todo
 * 👉Create a component that accepts two properties: `firstName` and `lastName`.
 * Assuming the first name is "John" and last name is "Doe" it should render a
 * `p` element with the text "Hello, John Doe!!".
 * 👉Render your component with an `id`. Pass the `id` to the `p` element.
 * TIP: use `Rest Syntax` and `Spread Operator`.
 */

ReactDOM.render(null, document.querySelector('#root'));
